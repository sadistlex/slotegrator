# Тестовое задание для Slotegrator 



## Описание

API тесты реализованы с помощью Rest Assured + JUnit 5\
UI Тесты реализованы в двух вариантах, первый более привычный для меня - Selenium + JUnit 5, вторая реализация на Selenium + Cucumber\
Все тест кейсы можно запускать в любом порядке.

## Как запустить

Для запуска тестов можно либо запустить их из IDE нажав правым кликом на папке java -> Run 'All Tests', либо на конкретный package - \
JUnit тесты в папке test/java/testCases\
Cucumber тесты в папке test/resources/features

Так-же можно запустить тесты из командной строки с помощью команды\
```mvn test -Dtest=*Cases*```
