package steps;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import pages.AdminPanelPage;
import pages.LoginPage;
import pages.PlayersPage;
import pages.sharedContent.Page;
import settings.TestControl;
import settings.WebDriverSettings;
import testCases.TestCase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static settings.Constants.*;

@ExtendWith(TestControl.class)
public class StepDefinitions extends TestCase {

    @Before
    public void init() {
        WebDriverSettings.setup();
        page = new Page();
        loginPage = new LoginPage();
        adminPanelPage = new AdminPanelPage();
        playersPage = new PlayersPage();
    }

    @After
    public void quit() {
        getDriver().quit();
    }

    @Given("Пользователь находится на странице логина")
    public void userIsOnLoginPage() {
        page.open(ADMIN_LOGIN_URL);
    }

    @When("Пользователь вводит валидный логин и пароль")
    public void userEntersValidLoginPassword() {
        loginPage.loginInput.sendKeys(ACCOUNT_LOGIN);
        loginPage.passwordInput.sendKeys(ACCOUNT_PASSWORD);
    }

    @And("Нажимает кнопку Войти")
    public void userClicksOnSignInBtn() {
        page.click(loginPage.signInBtn);
    }

    @Then("Пользователь успешно авторизован, админ-панель загрузилась")
    public void userSuccessfullyAuthenticatedAdminPanelVisible() {
        page.assertElementVisible(adminPanelPage.mainSection);
    }

    @Given("Залогиненный пользователь находится на главной странице Админ Панели")
    public void userAtMainAdminPage() {
        page.open(ADMIN_LOGIN_URL);
        loginPage.login();
    }

    @When("Нажимает на Список пользователей")
    public void userClicksOnPlayerList() {
        page.click(adminPanelPage.playersListBtn);
    }

    @Then("Таблица с игроками загрузилась")
    public void playersTableLoaded() {
        page.assertElementVisible(playersPage.playersTable);
        Assertions.assertFalse(playersPage.getTableRows().isEmpty(), "Таблица пустая");
    }

    @Given("Пользователь находится на странице Списка игроков")
    public void userAtPlayerListPage() {
        page.open(ADMIN_LOGIN_URL);
        loginPage.login();
        page.navigate(PLAYERS_LIST_URL);
        page.assertElementVisible(playersPage.playersTable);
    }

    @When("^Нажимает на (\\d) по счету столбец$")
    public void userClicksOnColumnToSort(int columnNumber) {
        playersPage.sortByColumn(columnNumber);
    }

    @Then("^Таблица верно отсортирована по выбранному столбцу (\\d)$")
    public void tableCorrectlySortedByColumn(int columnNumber) {
        List<String> columnAfterSort = playersPage.getAllColumnValues(columnNumber);
        List<String> columnAfterManualSort = new ArrayList<>(columnAfterSort);
        Collections.sort(columnAfterManualSort);
        System.out.println(columnAfterManualSort);
        Assertions.assertEquals(columnAfterSort, columnAfterManualSort, "Сортировка не совпадает с ожидаемой");
    }

}
