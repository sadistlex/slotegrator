package testCases;

import io.github.artsok.RepeatedIfExceptionsTest;
import pages.*;
import pages.sharedContent.Page;
import settings.Constants;
import settings.TestControl;
import settings.WebDriverSettings;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;


@ExtendWith(TestControl.class)
public class TestCase extends WebDriverSettings {

    protected static Page page;
    protected static LoginPage loginPage;
    protected static AdminPanelPage adminPanelPage;
    protected static PlayersPage playersPage;

    final static long suspend = 5000L;
    final static int repeats = 1;

}
