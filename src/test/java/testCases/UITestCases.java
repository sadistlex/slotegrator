package testCases;

import io.github.artsok.ParameterizedRepeatedIfExceptionsTest;
import io.github.artsok.RepeatedIfExceptionsTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pages.AdminPanelPage;
import pages.LoginPage;
import pages.PlayersPage;
import pages.sharedContent.Page;
import settings.WebDriverSettings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static settings.Constants.*;

public class UITestCases extends TestCase {

    @BeforeEach
    public void init () {
        WebDriverSettings.setup();
        page = new Page();
        loginPage = new LoginPage();
        adminPanelPage = new AdminPanelPage();
        playersPage = new PlayersPage();
    }

    @RepeatedIfExceptionsTest(name = "Авторизоваться в админке", suspend = suspend, repeats = repeats)
    @DisplayName("Авторизоваться в админке")
    public void authorizeTest() {
        page.open(ADMIN_LOGIN_URL);
        loginPage.loginInput.sendKeys(ACCOUNT_LOGIN);
        loginPage.passwordInput.sendKeys(ACCOUNT_PASSWORD);
        page.click(loginPage.signInBtn);
        page.assertElementVisible(adminPanelPage.mainSection);
    }

    @RepeatedIfExceptionsTest(name = "Открыть список игроков", suspend = suspend, repeats = repeats)
    @DisplayName("Открыть список игроков")
    public void openPlayersListTest() {
        page.open(ADMIN_LOGIN_URL);
        loginPage.login();
        page.click(adminPanelPage.playersListBtn);
        page.assertElementVisible(playersPage.playersTable);
        Assertions.assertFalse(playersPage.getTableRows().isEmpty(), "Таблица пустая");
    }

    @ParameterizedRepeatedIfExceptionsTest(name = "Отсортировать по столбцу {0}", suspend = suspend, repeats = repeats)
    @ValueSource(ints = {1, 2, 9})
    @DisplayName("Отсортировать по любому столбцу")
    public void playersSortTest(int columnNumber) {
        page.open(ADMIN_LOGIN_URL);
        loginPage.login();
        page.navigate(PLAYERS_LIST_URL);
        page.assertElementVisible(playersPage.playersTable);
        List<String> columnBeforeSort = playersPage.getAllColumnValues(columnNumber);
        playersPage.sortByColumn(columnNumber);
        List<String> columnAfterSort = playersPage.getAllColumnValues(columnNumber);
        Assertions.assertNotEquals(columnBeforeSort, columnAfterSort, "Сортировка не изменилась");
        List<String> columnAfterManualSort = new ArrayList<>(columnAfterSort);
        Collections.sort(columnAfterManualSort);
        Assertions.assertEquals(columnAfterSort, columnAfterManualSort, "Сортировка не совпадает с ожидаемой " + columnAfterManualSort);
    }

}
