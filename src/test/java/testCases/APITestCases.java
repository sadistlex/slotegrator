package testCases;

import helpers.DataGenerator;
import helpers.Http;
import io.github.artsok.ParameterizedRepeatedIfExceptionsTest;
import io.github.artsok.RepeatedIfExceptionsTest;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.Matchers.*;
import static settings.Constants.*;

import java.util.Map;
import java.util.stream.Stream;

public class APITestCases extends TestCase {

    static Stream<Map<String, String>> optionalFields() {
        //Необязательные поля при создании игрока
        return Stream.of(
                Map.of(),
                Map.of(
                        NAME_JSON, "John",
                        SURNAME_JSON, "Doe",
                        CURRENCY_CODE_JSON, "RUB"
                )
        );
    }

    @RepeatedIfExceptionsTest(name = "Получить токен гостя", suspend = suspend, repeats = repeats)
    @DisplayName("Получить токен гостя")
    public void clientCredentialsGrantTest() {
        ValidatableResponse response = Http.requestTokenPOST(Map.of(
                GRANT_TYPE_JSON, "client_credentials",
                SCOPE_JSON, "guest:default"
        ));
        response.statusCode(200);
        response.body(ACCESS_TOKEN_JSON, not(empty()));
    }

    @ParameterizedRepeatedIfExceptionsTest(name = "Зарегистрировать игрока с необязательными полями: {0}", suspend = suspend, repeats = repeats)
    @MethodSource("testCases.APITestCases#optionalFields")
    @DisplayName("Зарегистрировать игрока")
    public void registerNewPlayerTest(Map<String, String> optionalFields) {
        String username = DataGenerator.generateRandomUsername("tester");
        Map<String, String> newPlayerInfo = new java.util.HashMap<>(Map.of(
                USERNAME_JSON, username,
                PASSWORD_CHANGE_JSON, PASSWORD_BASE64,
                PASSWORD_REPEAT_JSON, PASSWORD_BASE64,
                EMAIL_JSON, username + "@gmail.com"
        ));
        newPlayerInfo.putAll(optionalFields);
        ValidatableResponse response = Http.requestPlayersPOST(newPlayerInfo);
        response.statusCode(201);
        response.body(PLAYER_ID_JSON, instanceOf(Integer.class));
        response.body("$", hasKey(COUNTRY_ID_JSON));
        response.body(COUNTRY_ID_JSON, equalTo(null));
        response.body("$", hasKey(TIMEZONE_ID_JSON));
        response.body(TIMEZONE_ID_JSON, equalTo(null));
        response.body("$", hasKey(USERNAME_JSON));
        response.body(USERNAME_JSON, equalTo(newPlayerInfo.get(USERNAME_JSON)));
        response.body("$", hasKey(EMAIL_JSON));
        response.body(EMAIL_JSON, equalTo(newPlayerInfo.get(EMAIL_JSON)));
        response.body("$", hasKey(NAME_JSON));
        response.body(NAME_JSON, equalTo(newPlayerInfo.get(NAME_JSON)));
        response.body("$", hasKey(SURNAME_JSON));
        response.body(SURNAME_JSON, equalTo(newPlayerInfo.get(SURNAME_JSON)));
        response.body("$", hasKey(GENDER_JSON));
        response.body(GENDER_JSON, equalTo(null));
        response.body("$", hasKey(PHONE_NUM_JSON));
        response.body(PHONE_NUM_JSON, equalTo(null));
        response.body("$", hasKey(BIRTHDATE_JSON));
        response.body(BIRTHDATE_JSON, equalTo(null));
        response.body(BONUSES_ALLOWED_JSON, equalTo(true));
        response.body(IS_VERIFIED_JSON, equalTo(false));
    }

    @RepeatedIfExceptionsTest(name = "Авторизоваться под созданным игроком", suspend = suspend, repeats = repeats)
    @DisplayName("Авторизоваться под созданным игроком")
    public void resourceOwnerPasswordCredentialsGrantTest() {
        String registeredUsername = Http.registerNewPlayer().path(USERNAME_JSON);
        ValidatableResponse response = Http.requestTokenPOST(Map.of(
                GRANT_TYPE_JSON, "password",
                USERNAME_JSON, registeredUsername,
                PASSWORD_JSON, PASSWORD_BASE64
        ));
        response.statusCode(200);
        response.body(ACCESS_TOKEN_JSON, not(empty()));
    }

    @ParameterizedRepeatedIfExceptionsTest(name = "Запросить данные профиля игрока c дополнительными полями: {0}", suspend = suspend, repeats = repeats)
    @MethodSource("testCases.APITestCases#optionalFields")
    @DisplayName("Запросить данные профиля игрока")
    public void getCurrentPlayerProfileInfo(Map<String, String> optionalFields) {
        Response createdPlayer = Http.registerNewPlayer(optionalFields);
        String authToken = Http.getPlayerToken(createdPlayer.path(USERNAME_JSON));
        ValidatableResponse response = Http.requestPlayersGET(authToken, createdPlayer.path(PLAYER_ID_JSON));
        response.statusCode(200);
        response.body(PLAYER_ID_JSON, instanceOf(Integer.class));
        response.body("$", hasKey(COUNTRY_ID_JSON));
        response.body(COUNTRY_ID_JSON, equalTo(null));
        response.body("$", hasKey(TIMEZONE_ID_JSON));
        response.body(TIMEZONE_ID_JSON, equalTo(null));
        response.body("$", hasKey(USERNAME_JSON));
        response.body(USERNAME_JSON, equalTo(createdPlayer.path(USERNAME_JSON)));
        response.body("$", hasKey(EMAIL_JSON));
        response.body(EMAIL_JSON, equalTo(createdPlayer.path(EMAIL_JSON)));
        response.body("$", hasKey(NAME_JSON));
        response.body(NAME_JSON, equalTo(createdPlayer.path(NAME_JSON)));
        response.body("$", hasKey(SURNAME_JSON));
        response.body(SURNAME_JSON, equalTo(createdPlayer.path(SURNAME_JSON)));
        response.body("$", hasKey(GENDER_JSON));
        response.body(GENDER_JSON, equalTo(null));
        response.body("$", hasKey(PHONE_NUM_JSON));
        response.body(PHONE_NUM_JSON, equalTo(null));
        response.body("$", hasKey(BIRTHDATE_JSON));
        response.body(BIRTHDATE_JSON, equalTo(null));
        response.body(BONUSES_ALLOWED_JSON, equalTo(true));
        response.body(IS_VERIFIED_JSON, equalTo(false));
    }

    @ParameterizedRepeatedIfExceptionsTest(name = "Запросить данные профиля игрока с id {0}", suspend = suspend, repeats = repeats)
    @ValueSource(ints = {1, 10000, 50000})
    @DisplayName("Запросить данные профиля игрока")
    public void getCurrentPlayerProfileInfo(int playerID) {
        Response createdPlayer = Http.registerNewPlayer();
        String authToken = Http.getPlayerToken(createdPlayer.path(USERNAME_JSON));
        ValidatableResponse response = Http.requestPlayersGET(authToken, playerID);
        response.statusCode(404);
    }


}
