package settings;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.util.Optional;


public class TestControl implements TestWatcher {

    @Override
    public void testSuccessful(ExtensionContext context) {
        String testMethod = context.getTestMethod().orElseThrow().getName();
        System.out.println("Test " + testMethod + " finished (success)");
        if (WebDriverSettings.getDriver() != null) {
            WebDriverSettings.getDriver().quit();
        }
    }

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        System.err.println("Test " + context.getTestMethod().orElseThrow().getName() + " disabled because of " + reason);
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        String testMethod = context.getTestMethod().orElseThrow().getName();
        System.out.println("Test " + testMethod + " finished (aborted)");
        cause.printStackTrace();
        if (WebDriverSettings.getDriver() != null) {
            WebDriverSettings.getDriver().quit();
        }
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        String testMethod = context.getTestMethod().orElseThrow().getName();
        System.out.println("Test " + testMethod + " finished (failed)");
        System.err.println(cause.getMessage());
        if (WebDriverSettings.getDriver() != null) {
            WebDriverSettings.getDriver().quit();
        }
    }



}
