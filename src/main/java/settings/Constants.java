package settings;

public class Constants {

    //UI Constants
    public static final String MAIN_URL = "http://test-app.d6.dev.devcaz.com";
    public static final String ADMIN_LOGIN_URL = "/admin/login";
    public static final String PLAYERS_LIST_URL = "/user/player/admin";

    public static final String ACCOUNT_LOGIN = "admin1";
    public static final String ACCOUNT_PASSWORD = "[9k<k8^z!+$$GkuP";

    //API Constants
    public static final String API_URL = "http://test-api.d6.dev.devcaz.com";

    public static final String HTTP_AUTH_USERNAME = "front_2d6b0a8391742f5d789d7d915755e09e";

    public static final String TOKEN_REQUEST_PATH = "/v2/oauth2/token";
    public static final String PLAYERS_REQUEST_PATH = "/v2/players";

    public static final String PASSWORD_BASE64 = "MTIzMTIz";

    public static final String GRANT_TYPE_JSON = "grant_type";
    public static final String SCOPE_JSON = "scope";
    public static final String TOKEN_TYPE_JSON = "token_type";
    public static final String EXPIRES_IN_JSON = "expires_in";
    public static final String ACCESS_TOKEN_JSON = "access_token";
    public static final String REFRESH_TOKEN_JSON = "refresh_token";

    //Поля игрока
    public static final String USERNAME_JSON = "username";
    public static final String PASSWORD_JSON = "password";
    public static final String PASSWORD_CHANGE_JSON = "password_change";
    public static final String PASSWORD_REPEAT_JSON = "password_repeat";
    public static final String EMAIL_JSON = "email";
    public static final String NAME_JSON = "name";
    public static final String SURNAME_JSON = "surname";
    public static final String CURRENCY_CODE_JSON = "currency_code";
    public static final String PLAYER_ID_JSON = "id";
    public static final String COUNTRY_ID_JSON = "country_id";
    public static final String TIMEZONE_ID_JSON = "timezone_id";
    public static final String GENDER_JSON = "gender";
    public static final String PHONE_NUM_JSON = "phone_number";
    public static final String BIRTHDATE_JSON = "birthdate";
    public static final String BONUSES_ALLOWED_JSON = "bonuses_allowed";
    public static final String IS_VERIFIED_JSON = "is_verified";



}
