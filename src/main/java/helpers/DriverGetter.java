package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.sharedContent.Page;

public class DriverGetter {

    public WebDriver driver;
    public WebDriverWait wait;
    public Actions action;
    public Page pageInner;

}
