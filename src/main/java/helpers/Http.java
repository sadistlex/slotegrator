package helpers;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import settings.Constants;


import java.util.Map;

import static io.restassured.RestAssured.given;
import static settings.Constants.*;

public class Http {

    private static RequestSpecification requestSpec = RestAssured.given().baseUri(API_URL);

    public static ValidatableResponse requestTokenPOST(Map<String, String> body) {
        System.out.println("Making POST request to " + Constants.TOKEN_REQUEST_PATH + " with " + body);
        ValidatableResponse response = requestSpec
                .auth()
                .preemptive()
                .basic(Constants.HTTP_AUTH_USERNAME, "")
                .contentType(ContentType.JSON)
                .body(body)
                .when()
                .post(Constants.TOKEN_REQUEST_PATH)
                .then();
        System.out.println(response.extract().response().asPrettyString());
        return response;
    }

    public static ValidatableResponse requestPlayersPOST(String authToken, Map<String, String> body) {
        System.out.println("Making POST request to " + Constants.PLAYERS_REQUEST_PATH + " with " + body + " \nand token " + authToken);
        ValidatableResponse response = requestSpec
                .auth()
                .preemptive()
                .oauth2(authToken)
                .contentType(ContentType.JSON)
                .body(body)
                .when()
                .post(Constants.PLAYERS_REQUEST_PATH)
                .then();
        System.out.println(response.extract().response().asPrettyString());
        return response;
    }

    public static ValidatableResponse requestPlayersPOST(Map<String, String> body) {
        return requestPlayersPOST(getGuestToken(), body);
    }

    public static ValidatableResponse requestPlayersGET(String authToken, int playerID) {
        System.out.println("Making GET request to " + Constants.PLAYERS_REQUEST_PATH + " for player  " + playerID + " \nand token " + authToken);
        ValidatableResponse response = requestSpec
                .auth()
                .preemptive()
                .oauth2(authToken)
                .contentType(ContentType.JSON)
                .when()
                .get(Constants.PLAYERS_REQUEST_PATH + "/" + playerID)
                .then();
        System.out.println(response.extract().response().asPrettyString());
        return response;
    }

    public static Response registerNewPlayer(Map<String, String> optionalFields) {
        //Создает нового пользователя и возвращает его username
        System.out.println("Creating new player");
        String username = DataGenerator.generateRandomUsername("tester");
        Map<String, String> newPlayerInfo = new java.util.HashMap<>(Map.of(
                USERNAME_JSON, username,
                PASSWORD_CHANGE_JSON, PASSWORD_BASE64,
                PASSWORD_REPEAT_JSON, PASSWORD_BASE64,
                EMAIL_JSON, username + "@gmail.com"
        ));
        newPlayerInfo.putAll(optionalFields);
        return requestPlayersPOST(newPlayerInfo).extract().response();
    }

    public static Response registerNewPlayer() {
        return registerNewPlayer(Map.of());
    }

    public static String getPlayerToken(String username) {
        Map<String, String> requestBody = Map.of(
                GRANT_TYPE_JSON, "password",
                USERNAME_JSON, username,
                PASSWORD_JSON, PASSWORD_BASE64
        );
        return getToken(requestBody);
    }

    public static String getGuestToken() {
        Map<String, String> requestBody = Map.of(
                GRANT_TYPE_JSON, "client_credentials",
                SCOPE_JSON, "guest:default"
        );
        return getToken(requestBody);
    }

    private static String getToken(Map<String, String> requestBody) {
        System.out.println("Requesting token with " + requestBody);

        ValidatableResponse response = requestTokenPOST(requestBody);
        String token = response.extract().response().path(ACCESS_TOKEN_JSON);
        System.out.println(token);
        return token;
    }

}
