package pages;

import helpers.DriverGetter;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.sharedContent.Page;
import settings.WebDriverSettings;

import java.util.ArrayList;
import java.util.List;

import static settings.WebDriverSettings.getDriver;

public class PlayersPage extends DriverGetter {

    public PlayersPage() {
        driver = getDriver();
        wait = WebDriverSettings.getWait();
        pageInner = new Page();
        PageFactory.initElements(driver,this);
    }

    //Таблица с игроками
    @FindBy(css = "div[id='payment-system-transaction-grid']")
    public WebElement playersTable;
    //Ряд в таблице
    public static final String tableRowLoc = "tbody tr";

    public List<WebElement> getTableRows() {
        System.out.println("Getting table rows");
        return getDriver().findElements(By.cssSelector(tableRowLoc));
    }

    public void sortByColumn(int columnNum) {
        System.out.println("Sorting by column number of " + columnNum);
        String locator = getColumnNameLocator(columnNum);
        pageInner.click(locator);
        try {
            wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(playersTable, "class", "loading")));
        }
        catch (StaleElementReferenceException | NoSuchElementException e) {
            System.out.println("Players table element is stale, meaning it finished loading");
        }
    }

    public List<String> getAllColumnValues(int columnNum) {
        System.out.println("Getting all column values with column number of " + columnNum);
        columnNum++; //Добавляем 1, потому что колонка ID не считается
        String locator = tableRowLoc + " td:nth-child(" + columnNum + ")";
        List<WebElement> elementsList = getDriver().findElements(By.cssSelector(locator));
        List<String> valuesList = new ArrayList<>();
        for (WebElement e : elementsList) {
            valuesList.add(e.getText());
        }
        System.out.println(valuesList);
        return valuesList;
    }

    private String getColumnNameLocator(int columnNum) {
        //Возвращает локатор названия столбца с выбранным порядковым номером
        return "th[id='payment-system-transaction-grid_c" + columnNum + "'] a";
    }


}
