package pages;

import helpers.DriverGetter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.sharedContent.Page;
import settings.WebDriverSettings;

import static settings.Constants.*;


public class LoginPage extends DriverGetter {

    public LoginPage() {
        driver = WebDriverSettings.getDriver();
        wait = WebDriverSettings.getWait();
        pageInner = new Page();
        PageFactory.initElements(driver,this);
    }

    //Инпут логина
    @FindBy(css = "input[id='UserLogin_username']")
    public WebElement loginInput;
    //Инпут пароля
    @FindBy(css = "input[id='UserLogin_password']")
    public WebElement passwordInput;
    //Кнопка войти
    @FindBy(css = "input[type='submit']")
    public WebElement signInBtn;

    public void login() {
        loginInput.sendKeys(ACCOUNT_LOGIN);
        passwordInput.sendKeys(ACCOUNT_PASSWORD);
        pageInner.click(signInBtn);
        wait.until(ExpectedConditions.not(ExpectedConditions.urlContains(ADMIN_LOGIN_URL)));
    }


}
