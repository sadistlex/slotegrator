package pages;

import helpers.DriverGetter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.sharedContent.Page;
import settings.WebDriverSettings;

public class AdminPanelPage extends DriverGetter {

    public AdminPanelPage() {
        driver = WebDriverSettings.getDriver();
        wait = WebDriverSettings.getWait();
        pageInner = new Page();
        PageFactory.initElements(driver,this);
    }

    //Секция с основным контентом
    @FindBy(css = "section[id='content']")
    public WebElement mainSection;

    //Кнопка Список пользователей
    @FindBy(css = "a[href='/user/player/admin'] div")
    public WebElement playersListBtn;
}
